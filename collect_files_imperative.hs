#!/usr/bin/env runhaskell
{-# LANGUAGE OverloadedStrings #-}

import qualified Text.Regex.PCRE.Heavy as Re
import Data.ByteString.Char8 (pack)
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import qualified System.Directory as Dir
import Control.Exception (catch)
import Options.Applicative
import Control.Monad

{- Gets list of observations for cluster located at the path given by
 - `cluster_source`
 -}
getObservations :: FilePath -> IO [String]
getObservations cluster_source = do
    let cache_file = cluster_source ++ "/cache"
    file_lines <- T.lines <$> TIO.readFile cache_file
    let obsid_lists =
            flip map (filter is_obsid_line file_lines) $
                map T.strip . T.split (==',') . T.drop 7
    case obsid_lists of
        list:_ -> return $! map T.unpack list
        _ -> ioError . userError $ "Could not read obsids from " ++ cache_file
    where
        is_obsid_line l =
            "Obsids:" `T.isPrefixOf` l || "obsids:" `T.isPrefixOf` l

{- Copies files matching a compiled regex in `regexes` from `source` to
 - `dest`
 -}
collect :: FilePath -> FilePath -> [Re.Regex] -> IO ()
collect source dest regexes = do
    files <- Dir.listDirectory source
    forM_ files $ \file ->
        when (any (file Re.=~) regexes) $ do
            Dir.createDirectoryIfMissing True dest
            Dir.copyFile (source ++ '/' : file) (dest ++ '/' : file)

{- Collects files matching the regexes in `regexes` from `cluster_path`
 - and `obs_path`, and places them in `out_path`
 -}
collectFiles :: FilePath -> FilePath -> FilePath -> [Re.Regex] -> IO ()
collectFiles cluster_path obs_path out_path regexes = do
    clusters <- Dir.listDirectory cluster_path
    forM_ clusters $ \cluster -> do
        let cluster_source = cluster_path ++ '/' : cluster
            cluster_dest = out_path ++ '/' : cluster
        collect cluster_source cluster_dest regexes
        observations <- catch (getObservations cluster_source) $ \err ->
            print (err :: IOError) >> return []
        forM_ observations $ \observation -> do
            let obs_source = obs_path ++ '/' : observation
            chips <- Dir.listDirectory obs_source
            forM_ chips $ \chip -> do
                let chip_source = obs_source ++ '/' : chip
                    chip_dest = cluster_dest ++ '/' : observation ++ '_' : chip
                collect chip_source chip_dest regexes

-- This is the equivalent of an argparse.ArgumentParser
data Options = Options {
    path :: FilePath,
    output :: FilePath,
    regexList :: [String]
}

{- Parse the command line arguments into an Options
 -}
parseArgs :: IO Options
parseArgs = do
    opts <- execParser (info (helper <*> parse) fullDesc)
    let cluster_path = path opts ++ "/clusters"
        obs_path = path opts ++ "/observations"
        out_path = output opts
    assertIO (Dir.doesDirectoryExist cluster_path) $
        "Cluster path does not exist: " ++ cluster_path
    assertIO (Dir.doesDirectoryExist obs_path) $
        "Observation path does not exist: " ++ obs_path
    assertIO (not <$> Dir.doesDirectoryExist out_path) $
        "Output path already exists: " ++ out_path
    return opts
    where
        parse = Options
            <$> strOption (
                    short 'p' <> long "path"
                    <> value "."
                    <> help "path containing clusters and observations"
                )
            <*> strOption (
                    short 'o' <> long "output"
                    <> value "collected"
                    <> help "path in which to place collected results"
            )
            <*> some (argument str $
                    metavar "REGEXES..."
                    <> help "collect files which match these regexes"
                )
        assertIO check err_msg = do
            bool <- check
            unless bool $ error err_msg

main :: IO ()
main = do
    opts <- parseArgs
    let regexes = map compile $ regexList opts
        cluster_path = path opts ++ "/clusters"
        obs_path = path opts ++ "/observations"
    collectFiles cluster_path obs_path (output opts) regexes
    where
        compile :: String -> Re.Regex
        compile re = either badregex id $ Re.compileM (pack re) []
        badregex err = error $ "Invalid regex" ++ show err
