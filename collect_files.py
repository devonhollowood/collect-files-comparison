#!/usr/bin/env python
''' Utility to collect files which match a regex from clusters and observations
'''

import os
import shutil
import re
import argparse

def get_observations(cluster_source):
    ''' Gets list of observations for cluster located at the path given by
        `cluster_source`
    '''
    cache_file = '{}/cache'.format(cluster_source)
    with open(cache_file) as handle:
        for line in handle.readlines():
            if line.startswith(('Obsids:', 'obsids:')):
                obsids = [obs.strip() for obs in line[7:].split(',')
                          if obs.strip()]
                return obsids
    raise RuntimeError('Could not read obsids from {}'.format(cache_file))

def collect(source, dest, regexes):
    ''' Copies files matching a compiled regex in `regexes` from `source` to
        `dest`. This should not be called on the same destination multiple
        times.
    '''
    # flag for whether or not we have created the dest directory yet
    for filename in os.listdir(source):
        for file_regex in regexes:
            if file_regex.match(filename):
                if not os.path.exists(dest):
                    os.makedirs(dest)
                full_source = '{}/{}'.format(source, filename)
                full_dest = '{}/{}'.format(dest, filename)
                shutil.copy2(full_source, full_dest)

def collect_files(cluster_path, obs_path, out_path, regexes):
    ''' Collects files matching the regexes in `regexes` from `cluster_path`
        and `obs_path`, and places them in `out_path`
    '''
    for cluster in os.listdir(cluster_path):
        # collect cluster-wide files
        cluster_source = '{}/{}'.format(cluster_path, cluster)
        cluster_dest = '{}/{}'.format(out_path, cluster)
        collect(cluster_source, cluster_dest, regexes)

        # collect observation-specific files
        try:
            observations = get_observations(cluster_source)
        except RuntimeError as err:
            print err
            continue
        for observation in observations:
            obs_source = '{}/{}'.format(obs_path, observation)
            for chip in os.listdir(obs_source):
                chip_source = '{}/{}'.format(obs_source, chip)
                chip_dest = '{}/{}_{}'.format(cluster_dest, observation, chip)
                collect(chip_source, chip_dest, regexes)

def parse_args():
    ''' Parses command line arguments
    '''
    parser = argparse.ArgumentParser(description='Makes images of clusters')
    parser.add_argument('-p', '--path', default='.',
                        help='path containing clusters and observations')
    parser.add_argument('-o', '--output', default='./collected',
                        help='path in which to place collected results')
    parser.add_argument('regexes', nargs='+',
                        help='collect files which match these regexes')
    options = parser.parse_args()
    # verify that paths are valid
    cluster_path = options.path + '/clusters'
    if not os.path.exists(cluster_path):
        exit('Cluster path does not exist: {}'.format(cluster_path))
    obs_path = options.path + '/observations'
    if not os.path.exists(obs_path):
        exit('Observation path does not exists: {}'.format(obs_path))
    # verify that output path does not already exist
    out_path = options.output
    if os.path.exists(out_path):
        exit('Output path already exists: {}'.format(out_path))
    return options

def main():
    ''' Run the program as a script
    '''
    # parse arguments
    options = parse_args()
    # compile regexes
    regexes = [re.compile(regex) for regex in options.regexes]
    cluster_path = options.path + '/clusters'
    obs_path = options.path + '/observations'
    collect_files(cluster_path, obs_path, options.output, regexes)

if __name__ == '__main__':
    main()
