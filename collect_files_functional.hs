#!/usr/bin/env runhaskell
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

import qualified Text.Regex.PCRE.Heavy as Re
import qualified Path as P
import Path ((</>))
import qualified Path.IO as PIO
import System.FilePath (dropTrailingPathSeparator)
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import qualified Data.ByteString.Char8 as B
import qualified Options.Applicative as Opt
import Control.Applicative
import Control.Monad (unless)
import Data.Monoid ((<>))

main :: IO ()
main = do
    (Paths{..}, regexes) <- getOpts
    cluster_dirs <- getSubDirs clusterPath
    file_pairs <- concat <$> mapM (processCluster observationPath) cluster_dirs
    let matching_pairs = filter (matches regexes . fst) file_pairs
    let final_pairs = fmap2 (outputPath </>) matching_pairs
    mapM_ copyFile final_pairs

{- Processing functions -}

processCluster :: AbsDir -> AbsDir -> IO [(AbsFile, RelFile)]
processCluster observation_root cluster_dir = do
    file_list <- getFiles cluster_dir
    let name = dirname cluster_dir
    cluster_pairs <- mapM (addDest name) file_list
    cache_name <- P.parseRelFile "cache"
    cache_file <- PIO.makeAbsolute $ cluster_dir </> cache_name
    obsids <- getObsids cache_file
    obs_dirs <- fmap2 (observation_root </>) $ mapM P.parseRelDir obsids
    obs_pairs <- concat <$> mapM processObservation obs_dirs
    return $ cluster_pairs ++ fmap2 (P.dirname cluster_dir </>) obs_pairs

processObservation :: AbsDir -> IO [(AbsFile, RelFile)]
processObservation obs_dir =
    getSubDirs obs_dir >>= fmap concat . mapM (processChip $ dirname obs_dir)

processChip :: Obsid -> AbsDir -> IO [(AbsFile, RelFile)]
processChip obsid chip_dir =
    getFiles chip_dir >>= mapM (addDest $ obsid ++ '_':dirname chip_dir)

getObsids :: AbsFile -> IO [Obsid]
getObsids cache_file = do
    contents <- TIO.readFile $ P.toFilePath cache_file
    let lists = map get_obsids . filter is_obsid_line . T.lines $ contents
    case lists of
        (first:_) -> return first
        _ -> printErr >> return []
    where
        is_obsid_line l =
            "Obsids:" `T.isPrefixOf` l || "obsids:" `T.isPrefixOf` l
        get_obsids = map T.unpack . map T.strip . T.split (==',') . T.drop 7
        printErr = putStrLn $
            "Could not read obsids from " ++ P.toFilePath cache_file

{- General utility functions -}

-- Turn a source into a (source, destination) pair
addDest :: String -> AbsFile -> IO (AbsFile, RelFile)
addDest dest_dir source = do
    dest_path <- P.parseRelDir dest_dir
    return (source, dest_path </> P.filename source)

-- See if `file` matches any of `regexes`
matches :: [Re.Regex] -> AbsFile -> Bool
matches regexes file = any (filename file Re.=~) regexes
    where filename = P.toFilePath . P.filename

-- Copy from `src` to `dst`
copyFile :: (AbsFile, AbsFile) -> IO ()
copyFile (src, dst) = do
    PIO.createDirIfMissing True $ P.parent dst
    PIO.copyFile src dst

-- There is a level beyond fmap. I call it... fmap2
fmap2 :: (Functor f1, Functor f2) => (a -> b) -> f1 (f2 a) -> f1 (f2 b)
fmap2 = fmap . fmap

{- Path manipulation utility functions -}

getSubDirs :: AbsDir -> IO [AbsDir]
getSubDirs dir = fst <$> PIO.listDir dir

getFiles :: AbsDir -> IO [AbsFile]
getFiles dir = snd <$> PIO.listDir dir

dirname :: P.Path b P.Dir -> String
dirname = dropTrailingPathSeparator . P.toFilePath . P.dirname

{- Option Parsing -}

getOpts :: IO (Paths, [Re.Regex])
getOpts = do
    Options{..} <-
        Opt.execParser (Opt.info (Opt.helper <*> parseOpts) Opt.fullDesc)
    clusterPath <- pathOpt `addDir` "clusters" >>= PIO.makeAbsolute
    observationPath <- pathOpt `addDir` "observations" >>= PIO.makeAbsolute
    outputPath <- P.parseRelDir outputOpt >>= PIO.makeAbsolute
    let paths = Paths{..}
    validatePaths paths
    return (paths, map compile regexesOpt)
    where
        root `addDir` sub =
            (</>) <$> (P.parseRelDir root) <*> (P.parseRelDir sub)

parseOpts :: Opt.Parser Options
parseOpts = Options
    <$> Opt.strOption (
            Opt.short 'p' <> Opt.long "path"
            <> Opt.value "."
            <> Opt.help "path containing clusters and observations"
        )
    <*> Opt.strOption (
            Opt.short 'o' <> Opt.long "output"
            <> Opt.value "collected"
            <> Opt.help "path in which to place collected results"
    )
    <*> some (Opt.argument Opt.str $
            Opt.metavar "REGEXES..."
            <> Opt.help "collect files which match these regexes"
        )

validatePaths :: Paths -> IO ()
validatePaths Paths{..} = do
    assertIO (PIO.doesDirExist clusterPath) $
        "Cluster path does not exist: " ++ P.toFilePath clusterPath
    assertIO (PIO.doesDirExist observationPath) $
        "Observation path does not exist: " ++ P.toFilePath observationPath
    assertIO (not <$> PIO.doesDirExist outputPath) $
        "Output path already exists: " ++ P.toFilePath outputPath
    where
        assertIO :: IO Bool -> String -> IO ()
        assertIO check err_msg = do
            bool <- check
            unless bool $ error err_msg

compile :: String -> Re.Regex
compile re = either badregex id $ Re.compileM (B.pack re) []
    where
        badregex err = error $ "Invalid regex" ++ show err

{- Type Definintions -}

data Options = Options {
    pathOpt :: FilePath,
    outputOpt :: FilePath,
    regexesOpt :: [String]
}

data Paths = Paths {
    clusterPath :: AbsDir,
    observationPath :: AbsDir,
    outputPath :: AbsDir
}

type RelFile = P.Path P.Rel P.File
type AbsFile = P.Path P.Abs P.File
type AbsDir = P.Path P.Abs P.Dir
type Obsid = String
