# Collect Files Comparison
This is a demonstration of the differences between scripting in python, imperative-style Haskell, and functional-style Haskell. All three scripts should be equivalent in functionality, and can be run as scripts using `python` or `runhaskell`. I wrote all three scripts as I probably would realistically write them (i.e. the minimum required to both function and satisfy `pylint`/`hlint`).

The `test` directory is supplied in case you want to try out the code. For example, try `./collect_files.py -p test '^info\.txt$'`

# Notes on Specific Implementations

## Python
* This was noticeably faster to write, but it took a little longer to sort through all the runtime errors.
* This only uses stuff from the python standard library, which was a minor benefit, but sort of nice.

## Haskell (imperative)
* This was the second fastest writing-time wise, and had the least runtime errors.
* This was written to be almost line-by-line comparable to the python version. The resulting code was still fairly normal for imperative-style Haskell.
* Overall, I thought this was the most enjoyable to write.
* This uses the following libraries which are not in the (somewhat minimal) Haskell standard library:
    * `Text.Regex.PCRE.Heavy` from `pcre-heavy`
    * `Data.ByteString.Char8` from `bytestring`
    * `Data.Text` and `Data.Text.IO` from `text`
    * `System.Directory` from `directory`
    * `Options.Applicative` from `optparse-applicative`

## Haskell (functional)
* This took the longest to write, though they were all pretty quick.
* This (surprisingly) had more runtime errors than the imperative Haskell version, which I wouldn't have expected given the stronger typing inherent in using the `Path` library over just using strings as the path. Most of these errors came from converting `Path` back and forth from `Strings` incorrectly.
* Due to laziness, this actually processes files in the same way that the Python and imperative Haskell versions do--that is, it finds a file to move, moves it, and then goes on to the next file, instead of building up a huge list of `(source file, destination file)` pairs (which is how it might seem at first glance).
* This uses the following libraries which are not in the (somewhat minimal) Haskell standard library:
    * `Text.Regex.PCRE.Heavy` from `pcre-heavy`
    * `Path` from `path`
    * `Path.IO` from `path-io`
    * `System.FilePath` from `filepath`
    * `Data.ByteString.Char8` from `bytestring`
    * `Data.Text` and `Data.Text.IO` from `text`
    * `System.Directory` from `directory`
    * `Options.Applicative` from `optparse-applicative`

# Conclusion
In this case, I didn't really think that the extra type-safety and compile-time checking was worth the extra time it took to write either Haskell version. That said, this sort of code is very IO heavy, and is thus a use-case where Haskell has little chance to shine. Given this, I thought the Haskell code was surprisingly nice to write (and more concise than I was expecting).
